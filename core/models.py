from django.db import models
from django.core.urlresolvers import reverse


class Segmento(models.Model):
    segmento = models.CharField('Segmento', max_length=100)
    slug = models.SlugField('Identificador', max_length=100)
    created = models.DateTimeField('Criado em', auto_now_add=True)
    modified = models.DateTimeField('Modificado em', auto_now=True)

    class Meta:
        verbose_name = 'Segmento'
        verbose_name_plural = 'Segmentos'
        ordering = ['segmento']

    def __str__(self):
        return self.segmento

    def get_absolute_url(self):
        return reverse('Segmento', kwargs={'slug': self.slug})

class vendas(models.Model):
    grupo = models.CharField('grupo', max_length=100)
    slug = models.SlugField('Identificador', max_length=100)
    prazo = models.IntegerField('prazo')
    bem = models.FloatField('bem', max_length=100)
    parcela = models.FloatField('parcela', max_length=100)
    descricao = models.CharField('descricao', max_length=100)
    segmento = models.ForeignKey('Segmento', verbose_name='Segmento')
    created = models.DateTimeField('Criado em', auto_now_add=True)
    modified = models.DateTimeField('Modificado em', auto_now=True)

    class Meta:
        verbose_name = 'detalhe'
        verbose_name_plural = 'detalhes'
        ordering = ['grupo']

    def __str__(self):
        return self.grupo

    def get_absolute_url(self):
        return reverse('Detalhe', kwargs={'slug': self.slug})

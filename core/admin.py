from django.contrib import admin
from .models import Segmento, vendas


class SegmentoAdmin(admin.ModelAdmin):

    list_display = ['segmento', 'slug']
    list_filter = ['created', 'modified']


class VendasAdmin(admin.ModelAdmin):

    list_display = ['grupo', 'bem', 'prazo', 'parcela']
    list_filter = ['created', 'modified']


admin.site.register(Segmento, SegmentoAdmin)
admin.site.register(vendas, VendasAdmin)

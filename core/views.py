from django.shortcuts import render, get_object_or_404
from django.shortcuts import redirect
from core import views

from .models import vendas, Segmento
#from django.core.mail import send_mail
from django.conf import settings
from django import forms
from django.core.mail import send_mail, BadHeaderError
from django.http import HttpResponse, HttpResponseRedirect



def index(request, template_name='index.html'):
	plano = vendas.objects.all()
	context = {'plano': plano}

	return render(request, template_name, context)

def detalhe(request, slug):
    plano = vendas.objects.filter(slug=slug)
    context = {'plano': plano}
    return render(request, 'detalhe.html', context)



# Create your views here.

from django.conf.urls import url
from django.contrib import admin
from core import views as c

urlpatterns = [
    url(
        regex=r'^$',
        view=c.index, name='home'
    ),
    url(
        regex=r'^(?P<slug>[\w-]+)/$',
        view=c.detalhe,
        name='Detalhe'),
    url(r'^admin/', admin.site.urls),
]
